import java.util.*;

public class Solution
{
  private final static double INF = 10000.0;
  private static double min(double a, double b){return a<b?a:b;}
  public static boolean isSafe(double[][] graph)
  {
    int V = graph.length;
    double G[][] = new double[V][V];
    for(int i = 0; i < V; i++)
      for(int j = 0; j < V; j++)
        G[i][j] = graph[i][j];

    for(int k = 0; k < V; k++)
      for(int i = 0; i < V; i++)
        for(int j = 0; j < V; j++)
          G[i][j] = min(G[i][j], G[i][k] * G[k][j]);

    boolean ret = false;
    for(int i = 0; i < V; i++)
      ret = ret || G[i][i] < 1.0;

    return ret;
  }
  public static void main(String[] args)
  {
    int V, E;

    Scanner input = new Scanner(System.in);

    V = input.nextInt();
    E = input.nextInt();

    double g[][] = new double[V][V];
    for(double[] row : g) Arrays.fill(row, INF);

    for(int i = 0; i < V; i++) g[i][i] = 1.0;

    for(int i = 0; i < E; i++)
    {
      int a, b;
      double c;
      a = input.nextInt();
      b = input.nextInt();
      c = input.nextDouble();
      g[a][b] = c;
    }

    System.out.println(isSafe(g) ? "yes" : "no");
  }
}
