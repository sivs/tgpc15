import java.util.*;

public class Solution 
{
  private long[] fibSeq;
  private int N;
  private long sum = 0;
  public Solution(int N)
  {
    fibSeq = new long[N];
    fibSeq[0] = 1;
    fibSeq[1] = 1;
    this.N = N;
    fib(N-1);
  }

  public long fib(int n)
  {
    if(fibSeq[n] != 0) return fibSeq[n];
    return fibSeq[n] = fib(n-2) + fib(n-1); 
  }

  public long getFibSum()
  {
    fib(N-1);
    for(long e: fibSeq) sum += e;
    return sum;
  }
    
  public static void main(String[] args)
  { 
    Scanner inp = new Scanner(System.in);
    Solution fib = new Solution(inp.nextInt());
    System.out.println(fib.getFibSum());
  }
}
