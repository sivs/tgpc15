import java.util.*;

public class Solution
{
  public static void main(String[] args)
  {
    // Create a scanner to read input
    Scanner inp = new Scanner(System.in);
    // While there is more input
    while(inp.hasNextInt())
    {
      // Read a number from input
      int C = inp.nextInt();
      // Remove all it's even factors
      while(C % 2 == 0) C /= 2;
      // Subtract one, divide by four and print
      C -= 1;
      System.out.println(C / 4);
    }
  }
}
