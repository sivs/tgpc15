import java.util.*;

public class Password
{
  private static final int INV = -1;

  private final String input;
  private static boolean inPassword(char ch)
  {
    boolean isIn = (ch >> 3) % 2 == 0; 
    return isIn;
  }

  public Password(String input)
  {
    this.input = input;
  }

  public ArrayList<String> getSubstrings(String st)
  {
    ArrayList<String> subStrings = new ArrayList<String>();
    int stringLength = st.length();
    for(int subSLength = st.length(); subSLength > 0; subSLength--)
      for(int startPos = 0; 
          startPos + subSLength <= stringLength;
          startPos++)  
      {
        String substr = st.substring(startPos, startPos + subSLength);
        subStrings.add(substr); 
        //System.out.println("How many times does this run on: " + substr);
      }
    return subStrings;
  }

  public void findPasswords()
  {
    int subSequenceStart = INV; 
    char prevChar = 'A'; 

    ArrayList<String> subStrings = new ArrayList<String>();

    for(int i = 0; i < input.length(); i++)
    {
      char curChar = input.charAt(i);
      if(inPassword(curChar))
      {
        if(subSequenceStart == INV) subSequenceStart = i;
      }
      else
      {
        if(subSequenceStart != INV)
        {
          String substr = input.substring(subSequenceStart, i);
          subStrings.addAll(getSubstrings(substr)); 
     //     System.out.println("Found substring: " + substr);
          subSequenceStart = INV;
        }
      }
      prevChar = curChar;
    }

    if(subSequenceStart != INV)
      subStrings.addAll(getSubstrings(input.substring(
              subSequenceStart, input.length()))); 

    Map<String, Integer> m = new HashMap<String, Integer>();
    for(String s: subStrings)
    {
      Integer freq = m.get(s);
      m.put(s,(freq == null)?1:freq+1);
    }

    // Hack to remove duplicates
    HashSet hs = new HashSet();
    hs.addAll(subStrings);
    subStrings.clear();
    subStrings.addAll(hs);


    // Print out all substrings in sorted order
    Collections.sort(subStrings);
    for(String s: subStrings)
      System.out.println(s + " occurs " + m.get(s) + " times");
  }

  public static void main(String[] args)
  {
    Scanner inp = new Scanner(System.in);
    Password dis = new Password(inp.next());
    dis.findPasswords();
  }
}
