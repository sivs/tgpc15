import java.util.*;

public class Solution
{
  // Calculate gcd using the pythagorian algorithm
  public static long gcd(long a, long b) 
  {
    return (a % b == 0) ?  b : gcd(b, a%b);
  }

  public static void main(String[] args)
  {
    // Read the two numbers into longs
    long n = 0, m = 0;

    Scanner input = new Scanner(System.in);
    
    for(char ch: input.next().toCharArray())
      n = (n << 1) + (ch == '1' ? 1 : 0);

    for(char ch: input.next().toCharArray())
      m = (m << 1) + (ch == '1' ? 1 : 0);

    // Calculate the gcd and print it
    String result = "";
    for(long g = gcd(n,m); g>0;g>>=1)
      result += (g%2 == 0? "0" : "1");
   
    // Reverse the string 
    System.out.println(new StringBuilder(result).reverse().toString());
  }
}
