public class Product extends State
{
  public Product(int p, int m)
  {
    super(p, m);
  }

  public int getStore() { return super.getStore(); }
  public int getPrice() { return super.getMoney(); }
}
