public class State implements Comparable<State>
{
  private final int p;
  private final int m;
  public State(int p, int m)
  {
    this.p = p;
    this.m = m;
  }

  public int getStore()
  {
    return p;
  }
  public int getMoney()
  {
    return m;
  }

  public boolean equals(State other)
  {
    return m==other.m && p==other.p;
  }

  public int compareTo(State other)
  {
    if(p == other.p && m == other.m) return 0;
    if(p == other.p)
      return other.m - m;
    else 
      return other.p - p;
  }
}
