import java.util.*;

public class FraudSolution
{
  private static final int INF = 10000000;
  // We use this state as a symbol
  private static final State INV_STATE = new State(1337, 1337);

  public static int min(int a, int b){ return a < b ? a : b; }

  // index is location first and then money
  private static Map<State, Integer> minNoItems;

  // Map stored as a matrix
  private static int map[][];
  // Simply make the code look a bit nicer
  private static int costBetween(int a, int b) { return map[a][b]; }

  // Return the state reached when buying product p from state s
  private static State buy(Product p, State s)
  {
    // cost of driving and buying the product
    int costOfBuying = costBetween(p.getStore(), s.getStore()) + p.getPrice();
    // If we can't afford it return the invalid state
    if(costOfBuying > s.getMoney())
      return INV_STATE;
    // Otherwise return the correct state
    else
      return new State(p.getStore(), s.getMoney() - costOfBuying);
  }

  // Determine if s is a final state
  // A state is final if the cost of driving from the store
  // associated with this state back to work is exactly the
  // amount of money in the state
  private static boolean isFinal(State s)
  {
    return s.getMoney() == costBetween(s.getStore(), 0);
  }

  // Array to store all pruducts
  private static Product products[];

  // Returns Position and noOfItems
  private static int minNOfTransitions(State s)
  {
    // Retrieve value if already calculated
    if(minNoItems.containsKey(s)) return minNoItems.get(s);
   
    // This state is invalid so return infinity 
    if(s == INV_STATE) return INF;
    // In case we reach one of our base cases
    if(isFinal(s)) return 0;

    // The minimum number of transitions needed to reach this state
    int min = INF;
    // Loop over all products and picking the one
    // that requires the least amount of transitions to reach
    // from a base case.
    for(Product prod: products) 
      min = min(minNOfTransitions(buy(prod, s)) + 1, min);

    // Store our result so we don't have to recompute it
    minNoItems.put(s, min); 

    return min; 
  }

  public static void main(String[] args)
  {
    int nStores, nRoads, nProducts, M; 
    Scanner input = new Scanner(System.in);

    M             = input.nextInt(); 
    nProducts     = input.nextInt(); 
    nStores       = input.nextInt(); 
    nRoads        = input.nextInt(); 

    map = new int[nStores][nStores];

    // Initialize map
    for(int[] row: map) Arrays.fill(row, INF);
    for(int i = 0; i < nStores; i++) map[i][i] = 0; // It's free to drive from a store to itself

    // We use a map between PositionMoney and PostionNoItems to store the solutions 
    minNoItems = new TreeMap<State, Integer>(); 
   
    // Read the roads 
    for(int i = 0; i < nRoads; i++)
    {
      int A = input.nextInt();
      int B = input.nextInt();
      int C = input.nextInt();
     
      map[A][B] = map[B][A] = C;    
    }
    
    // Calculate the shortest paths using Floyd-Warshall
    // This is not quick enough for the actual competition
    // but this is not really the difficult part of the problem
    // and I want to minimize potential errors in the solution
    for(int k = 0; k < nStores; k++)
      for(int i = 0; i < nStores; i++)
        for(int j = 0; j < nStores; j++)
          map[i][j] = min(map[i][j], map[i][k] + map[k][j]); 

    // Read the products 
    products = new Product[nProducts];

    for(int i = 0; i < nProducts; i++)
      products[i] = new Product(input.nextInt(), input.nextInt());

    System.out.println(minNOfTransitions(new State(0, M)));
  }
}
