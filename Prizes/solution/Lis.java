public class Lis
{
  public static int getLis(int[] seq)
  {
    int[] lis = new int[seq.length];
    lis[0] = seq[0];
    int curIndex = 0;

    for(int i = 0; i < lis.length;i++)
    {
      if(seq[i] > lis[curIndex])
      {
        curIndex++;
        lis[curIndex] = seq[i];
      }
      else
      {
        int item = seq[i];
        int low = 0; int high = curIndex;
        int pos = (high + low) / 2;
        while((high - low) > 1)
        {
          pos = (high + low) / 2;
          if(item < lis[pos]) high = pos;
          else low = pos;
        }
        if((high - low) == 0) lis[pos] = item;
        else lis[high] = item;
      }
    }

    return curIndex + 1;
  }

  public static void main(String[] args)
  {
    int[] seq = {-7, 10, 9, 2, 3, 8, 8, 1, 2, 3, 4, 5,-4, 10};

    int[] lis = new int[seq.length];
    lis[0] = seq[0];
    int curIndex = 0;

    for(int i = 0; i < lis.length;i++)
    {
      if(seq[i] > lis[curIndex])
      {
        curIndex++;
        lis[curIndex] = seq[i];
        System.out.println("Adding: " + seq[i]);
      }
      else
      {
        System.out.println("Inserting: " + seq[i]);
        int item = seq[i];
        int low = 0; int high = curIndex;
        int pos = (high + low) / 2;
        while((high - low) > 1)
        {
          pos = (high + low) / 2;
          if(item < lis[pos]) high = pos;
          else low = pos;
        }
        if((high - low) == 0) lis[pos] = item;
        else lis[high] = item;

        for(int X : lis) System.out.print(" " + X);
        System.out.println();
      }
    }

    for(int X : lis) System.out.print(" " + X);
    System.out.println();
    System.out.println("Lis: " + (curIndex + 1) + "");
  }
}
